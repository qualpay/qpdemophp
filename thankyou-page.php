
<?php
include 'includes/header.php';
include 'includes/back.php';
?>
<div class="row text-large" align="center"> Process Payment</div>
<div class="row text-large" align="center">Demo - Checkout Page</div>
<div >&nbsp;</div>

<?php include 'includes/cart.php'?>
<br>

<?php
$msg = $_GET['msg'];
$amount = $_GET['amt']
?>

<div class="ng-scope" style="">
    <div class="row">
      <div class="grid-fourth">&nbsp;</div>
      <div class="grid-half">
    <div align="center" class="text-large">
      Thanks for your payment!
    </div>
    <div class="row">&nbsp;</div>
    <div class="row">
      <div class="grid-full receipt">
        <div class="row">&nbsp;</div>

        <div class="row">
          <div class="grid-fourth"><b>Amount:</b></div>
          <div class="grid-three-fourths ng-binding">$<?php echo round($amount, 2, PHP_ROUND_HALF_UP); ?></div>
        </div>

        <div class="row">
          <div class="grid-fourth"><b>Message:</b></div>
          <div class="grid-three-fourths ng-binding"><?php echo $msg; ?></div>
        </div>
        <div class="row">&nbsp;</div>
      </div>
    </div>
</div>
      </div>
    </div>
  </div>
  <?php
include 'includes/footer.php';
?>
