<?php
include 'includes/header.php';
include 'includes/back.php';
?>
<style>
  .qp-payment-container{
    position: relative;
    padding-bottom: 30%;
    padding-top: 25px;
    height: 0;
    overflow: hidden;
  }
  .qp-payment-container iframe {
    position: absolute;
    top:0;
    left: 0;
    width: 100%;
    height: 100%;
  }
</style>

<div class="row text-large" align="center"> Process Payment</div>
<div class="row text-large" align="center">Demo - Embedded Fields</div>
<div >&nbsp;</div>

<?php include 'includes/cart.php'?>
<br>

<form id="demo-payment-form" class="qp-form" method="post" action="/">
  <div>
    <?php include 'includes/address.php'?>
  </div>
  <div id="continue-button" class="grid-full" align="center" style="display:block">
    <button type="button" class="btn btn-primary" onclick="showHideElement('payment'); showHideElement('continue-button'); ">Continue</button>
  </div>

 <div id="payment" class="row" style="display:none">


<?php
/**
 * A sample to demonstrate Qualpay embedded fields usage
 */
include 'includes/price.php';

require_once __DIR__ . '/lib/qpPlatform/SwaggerClient-php/vendor/autoload.php';
require_once __DIR__ . '/lib/qpPg/SwaggerClient-php/vendor/autoload.php';

include 'includes/property.php';

$mode = $transaction_mode;
$securityKey = $security_key;
$merchantId = $merchant_id;
$purchaseId = $purchase_id;
$qp_url = $url;

// Invoke API.
$config = new \qpPlatform\Configuration();

$config->setUsername($securityKey)
    ->setHost($qp_url . '/platform');

$http_client = new GuzzleHttp\Client();
$api_instance = new \qpPlatform\Api\EmbeddedFieldsApi($http_client, $config);

//Generate Transient Key Using Embedded Fieds API
$result = $api_instance->getEmbeddedTransientKey();
$transient_key = $result->getData()->getTransientKey();

// Load Embedded Container.
if (!empty($transient_key)) {

    $loadFrame = "<div class='row'><div class='grid-full'>&nbsp;</div></div>
    <div class='row'><div class='grid-fourth'>&nbsp;</div><div class='grid-half'>";

    $loadFrame .= '<link rel="stylesheet" type="text/css" href="https://app.qualpay.com/hosted/embedded/css/qp-embedded.css" />';
    $loadFrame .= '<script src="https://app.qualpay.com/hosted/embedded/js/qp-embedded-sdk.min.js"></script>';
    $loadFrame .= '<div id="qp-embedded-container" class = "row" align="center"> style="display:none"</div>';
    $loadFrame .= '<script type="text/javascript">var merchant_id  = "' . $merchant_id . '";</script>';
    $loadFrame .= '<script type="text/javascript">var key = "' . $transient_key . '";</script>';
    $loadFrame .= '<script type="text/javascript">var mode = "' . $mode . '";</script>';
    $loadFrame .= '<script type="text/javascript" src="js/load-frame-pg.js"></script>';
    $loadFrame .= "</div></div>";
    echo $loadFrame;

} else {
    echo "Unable to take payment";
}
?>
    <div class="row">&nbsp;</div>
    <div class="row">
      <div class="grid-full" align="center">
        <input class="btn btn-primary" type="submit" name="submit_button" value="Pay Now"></input>
      </div>
    </div>
  </div>

  <input type="hidden" id="amt_tran" name="amt_tran" value="<?php echo $amt_tran; ?>"/>
  <input type="hidden" id="purchase_id" name="purchase_id" value="<?php echo $purchaseId; ?>"/>
</form>
<?php
include 'includes/footer.php';
?>
