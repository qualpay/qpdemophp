
<?php
require_once __DIR__ . '/../lib/qpPlatform/SwaggerClient-php/vendor/autoload.php';

/**
 * Add a customer record to vault
 */

include ('../includes/property.php');

define('MOTO_ECOMM_ID', 7);
define('DEV_ID', 'Qualpay_Drupal7');

//Read property file
$qp_url = $url;
$securityKey = $security_key;
$merchantId = $merchant_id;
$txn_type = $transaction_type;

//Form data
$customer_first_name = $_POST['customer_first_name'];
$customer_last_name = $_POST['customer_last_name'];
$customer_phone     = $_POST['customer_phone'];
$customer_email     = $_POST['customer_email_address'];

$billing_first_name = $_POST['billing_first_name'];
$billing_last_name = $_POST['billing_last_name'];
$billing_addr1 = $_POST['billing_street_addr1'];
$billing_city = $_POST['billing_city'];
$billing_state = $_POST['billing_state'];
$billing_zip = $_POST['billing_zip'];
$billing_country = $_POST['billing_country'];



$card_id = $_POST['card_id'];

$billing_zip = (strlen($billing_zip) > 10) ? substr($billing_zip, 0, 9) : $billing_zip;

$moto_ecomm_ind = MOTO_ECOMM_ID;
$dev_id = DEV_ID;

//Configure API
$config = new \qpPlatform\Configuration();

$config->setUsername($securityKey)
    ->setHost($qp_url . "/platform");

// Build AddCustomer request
$http_client = new GuzzleHttp\client();
$api_instance = new \qpPlatform\Api\CustomerVaultApi($http_client, $config);

//Build shipping if shipping is provided
/*$
$shipping_first_name = $_POST['shipping_first_name'];
$shipping_last_name = $_POST['shipping_last_name'];
$shipping_addr1 = $_POST['shipping_street_addr1'];
$shipping_city = $_POST['shipping_city'];
$shipping_state = $_POST['shipping_state'];
$shipping_zip = $_POST['shipping_zip'];
$shipping_country = $_POST['shipping_country'];
shipping = new \qpPlatform\Model\AddShippingAddressRequest();
$shipping->setShippingFirstName($shipping_first_name)
->setShippingLastName($shipping_last_name)
->setShippingAddr1($shipping_addr1)
->setShippingCity($shipping_city)
->setShippingState($shipping_state)
->setShippingZip($shipping_zip)
->setShippingCountry($shipping_country);*/

//Build Billing data
$billing = new \qpPlatform\Model\AddBillingCardRequest();
$billing->setBillingFirstName($billing_first_name)
->setBillingLastName($billing_last_name)
->setBillingAddr1($billing_addr1)
->setBillingCity($billing_city)
->setBillingState($billing_state)
->setBillingZip($billing_zip)
->setBillingCountry($billing_country)
->setCardId($card_id);

$body = new \qpPlatform\Model\AddCustomerRequest();
$body->setAutoGenerateCustomerId(true)                 //Qualpay will auto generate customer id
     //->setCustomerId("JONDOE")                       //or set customer_id if you have your own id that you would like to use. Refer to documentation for allowed values in this field
     ->setCustomerFirstName($customer_first_name)      //Required
     ->setCustomerLastName($customer_last_name)        //Required
     ->setCustomerPhone($customer_phone)
     ->setCustomerEmail($customer_email)                  
    // ->setShippingAddresses(array($shipping))        
     ->setBillingCards(array($billing));               //Add billing information


//Invoke add customer API
try {
    $result = $api_instance->addCustomer($body);
    $code = $result->getCode();
    $msg = $result->getMessage();
    $data = $result->getData();

    echo $result;
    http_response_code(201);
 
} catch (Exception $e) {
    echo $e->getResponseBody();
    http_response_code(503);
}
?>
