<?php
  session_start();
  error_reporting(E_ERROR | E_WARNING | E_PARSE);
  class DemoPreferences{
    public $expire_in_secs = 300;
    public $email_receipt = false;
    public $success_url = 'https://www.qualpay.biz/demo/receipt.php';
    public $failure_url = '';
    public $notification_url = '';
    public $allow_partial_payments = false;
    public $request_type = 'sale';
  }
  define('DEFAULT_PREFERENCES', serialize(new DemoPreferences()));
  if (!isset($_SESSION['preferences'])) {
    $_SESSION['preferences'] = DEFAULT_PREFERENCES;
  }

  function getDemoPreferences(){
    return unserialize($_SESSION['preferences']);
  }

  function resetDemoPreferences(){
    $_SESSION['preferences'] = DEFAULT_PREFERENCES;
  }

  function updateDemoPreferences($pref){
    if (!empty($pref))
      $_SESSION['preferences'] = serialize($pref);
    else {
      $_SESSION['preferences'] = DEFAULT_PREFERENCES;
    }
  }
?>
<!DOCTYPE>
<html>
  <head>
    <link rel="stylesheet" href="css/main.css" type="text/css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
    <script   src="https://code.jquery.com/jquery-2.2.3.min.js"   integrity="sha256-a23g1Nt4dtEYOj7bR+vTu7+T8VP13humZFBJNIYoEJo="   crossorigin="anonymous"></script>
    <title>Qualpay Demo Application</title>
  </head>
  <body>
    <script src="https://app-dev.qualpay.com/hosted/embedded/js/qp-embedded-sdk.min.js"></script>
    <script>
      var showHideElement = function(id){
        if (id === 'undefined') return;
        var element = document.getElementById(id);
        if (element !== 'undefined' && element.style.display === 'block'){
          element.style.display = 'none';
        } else{
          element.style.display = 'block';
        }
      };
    </script>
    <div class="header">
      <img class="logo" src="images/Acme_logo_web.png" alt="Acme"/>
    </div>
    <div class="content">
