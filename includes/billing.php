<div class="row" id="billing">
  <div class="grid-sixth">&nbsp;</div>
  <div class="grid-two-thirds">
    <div class="qp-header corner-top" style="margin-top: 10px;">
      <div class="row">
        <div class="grid-full">Billing Information</div>
        <div class="clear"></div>
      </div>
    </div>
    <div class="qp-box corner-bottom" style="margin-bottom: 10px;">
      <div class="row">
        <div class="grid-half">
          <input type="text" name="billing_first_name" placeholder="First Name" value="John" maxlength=32></input> </div> <div
            class="grid-half">
          <input type="text" name="billing_last_name" placeholder="Last Name" value="Doe" maxlength=32></input>
        </div>
      </div>
      <div class="row">
        <div class="grid-full">
          <input type="text" name="billing_street_addr1" placeholder="Street Address" maxlength=64 value="4th Avenue"></input>
        </div>
      </div>
      <div class="row">
        <div class="grid-fourth">
          <input type="text" name="billing_city" placeholder="City" maxlength ="32" value="San Mateo"></input>
        </div>
        <div class="grid-fourth">
          <input type="text" name="billing_state" placeholder="State" maxlength ="32" value="CA"></input>
        </div>
        <div class="grid-fourth">
          <input type="text" name="billing_zip" placeholder="Zip" maxlength="10" value="95054" required></input>
        </div>
        <div class="grid-fourth">
          <input type="text" name="billing_country" placeholder="Country" maxlength="3" value="USA"></input>
        </div>
      </div>
    </div>
  </div>
</div>