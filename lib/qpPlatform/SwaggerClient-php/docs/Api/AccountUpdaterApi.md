# qpPlatform\AccountUpdaterApi

All URIs are relative to *https://api-dev.qualpay.com/platform*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addAusRequest**](AccountUpdaterApi.md#addAusRequest) | **POST** /aus/add | Submit AUS Request
[**getAusResponse**](AccountUpdaterApi.md#getAusResponse) | **GET** /aus/{requestId} | Get AUS Response


# **addAusRequest**
> \qpPlatform\Model\AusRequestResponse addAusRequest($body)

Submit AUS Request

Submit an Account Updater request using full card number and expiration date.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basicAuth
$config = qpPlatform\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new qpPlatform\Api\AccountUpdaterApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \qpPlatform\Model\AusRequest(); // \qpPlatform\Model\AusRequest | aus request

try {
    $result = $apiInstance->addAusRequest($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountUpdaterApi->addAusRequest: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\qpPlatform\Model\AusRequest**](../Model/AusRequest.md)| aus request |

### Return type

[**\qpPlatform\Model\AusRequestResponse**](../Model/AusRequestResponse.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getAusResponse**
> \qpPlatform\Model\AusUpdatedResponse getAusResponse($request_id)

Get AUS Response

Get updated AUS response using requestId

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure HTTP basic authorization: basicAuth
$config = qpPlatform\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new qpPlatform\Api\AccountUpdaterApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$request_id = 789; // int | Request ID

try {
    $result = $apiInstance->getAusResponse($request_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountUpdaterApi->getAusResponse: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **request_id** | **int**| Request ID |

### Return type

[**\qpPlatform\Model\AusUpdatedResponse**](../Model/AusUpdatedResponse.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

