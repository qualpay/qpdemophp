# AusRequestData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**card_number** | **string** | Full card number. | 
**exp_date** | **string** | Card expiration date in MMYY format. | 
**card_id** | **string** | Alphanumeric card id and can be up to 32 character. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


