# CheckoutSettings

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**allow_partial_payments** | **bool** | &lt;br&gt;&lt;strong&gt;Description: &lt;/strong&gt;If set to true, the customer can edit the transaction amount when making payment on the Invoice checkout page. Set this flag to true if you want to allow the customer to make partial payments. | [optional] 
**email_receipt** | **bool** | &lt;br&gt;&lt;strong&gt;Description: &lt;/strong&gt;If set to true and the customer email address is provided, Qualpay generates and sends a receipt to the customer once a payment is made on the invoice checkout page. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


